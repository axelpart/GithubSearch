//
//  MainDataManager.swift
//  GithubSearch
//
//  Created by Alexandros Partonas on 22.04.18.
//  Copyright © 2018 Alexandros Partonas. All rights reserved.
//

import Foundation

protocol MainDataProtocol {
    func searchGithub(with text: String, completion: @escaping (_ repositories: [Repository]?, _ error: Error?) -> ())
}

class MainDataManager: MainDataProtocol {
    
    func searchGithub(with text: String, completion: @escaping (_ repositories: [Repository]?, _ error: Error?) -> ()) {
        ApiProvider.shared.apiOperation(.search(text: text)) { (page: SearchResponse?, error: Error?) in
            guard let items = page?.items else { completion(nil, error); return }
            completion(items, nil)
        }
    }
}
