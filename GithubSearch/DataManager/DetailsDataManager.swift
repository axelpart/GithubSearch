//
//  DetailsDataManager.swift
//  GithubSearch
//
//  Created by Alexandros Partonas on 23.04.18.
//  Copyright © 2018 Alexandros Partonas. All rights reserved.
//

import Foundation

protocol DetailsDataProtocol {
    func getSubscribers(forOwner login: String, andRepoName name: String, completion: @escaping (_ subscribers: [User]?, _ error: Error?) -> ())
}

class DetailsDataManager: DetailsDataProtocol {
    
    func getSubscribers(forOwner login: String, andRepoName name: String, completion: @escaping (_ subscribers: [User]?, _ error: Error?) -> ()) {
        let request: ApiRequest = ApiRequest.getSubscribers(ownerLogin: login, repoName: name)
        ApiProvider.shared.apiOperation(request) { (subscribers: [User]?, error: Error?) in
            guard let subscribers = subscribers else { completion(nil, error); return }
            completion(subscribers, nil)
        }
    }
}
