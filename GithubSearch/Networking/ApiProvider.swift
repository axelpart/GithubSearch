//
//  ApiProvider.swift
//  GithubSearch
//
//  Created by Alexandros Partonas on 22.04.18.
//  Copyright © 2018 Alexandros Partonas. All rights reserved.
//

import Foundation

class ApiProvider {
    
    static let shared = ApiProvider()
    private let conversionError = NSError(domain: "conversion", code: 50000, userInfo: nil)
    
    func apiOperation<T: Decodable>(_ apiRequest: ApiRequest, completion: @escaping (T?, Error?) -> ()) {
        let task = urlDataTask(forApiRequest: apiRequest) { data, error in
            if let data = data {
                let (records, error) = T.decoded(from: data)
                completion(records, error)
            } else {
                completion(nil, error)
            }
        }
        task.resume()
    }
    
    private func urlDataTask(forApiRequest apiRequest: ApiRequest, completion: @escaping (_ responseData: Data?, _ error: Error?) -> ()) -> URLSessionDataTask {
        return URLSession.shared.dataTask(with: apiRequest.urlRequest, completionHandler: { (data, response, error) -> Void in
            guard error == nil else {
                print("Error while fetching data: \(String(describing: error?.localizedDescription))")
                DispatchQueue.main.async(execute: {
                    completion(nil, error)
                })
                return
            }
            
            guard let safeData = data else {
                print("Unknown error occurred with conversion...", terminator: "\n")
                DispatchQueue.main.async(execute: {
                    completion(nil, self.conversionError)
                })
                return
            }
            
            DispatchQueue.main.async(execute: {
                completion(safeData, nil)
            })
        })
    }
}
