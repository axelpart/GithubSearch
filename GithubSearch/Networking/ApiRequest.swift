//
//  ApiRequest.swift
//  GithubSearch
//
//  Created by Alexandros Partonas on 22.04.18.
//  Copyright © 2018 Alexandros Partonas. All rights reserved.
//

import Foundation

enum HttpMethod: String {
    case GET
    case PUT
    case POST
    case DELETE
    
    var description: String {
        return self.rawValue
    }
}

enum ApiRequest {
    static let baseURLPath = "https://api.github.com"
    
    case search(text: String)
    case getSubscribers(ownerLogin: String, repoName: String)
    
    internal var urlRequest: URLRequest {
        let result: (path: String, method: HttpMethod, parameters: [String: AnyObject]?) = {
            switch self {
            case .search(let text):
                let formattedText = text.replacingOccurrences(of: " ", with: "+")
                return ("/search/repositories?q=\(formattedText)", .GET, nil)
            case .getSubscribers(let ownerLogin, let repoName):
                return ("/repos/\(ownerLogin)/\(repoName)/subscribers", .GET, nil)
            }
        }()
        
        let url = URL(string: "\(ApiRequest.baseURLPath)\(result.path)")!
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = result.method.rawValue
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        return urlRequest
    }
}
