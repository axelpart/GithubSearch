//
//  User.swift
//  GithubSearch
//
//  Created by Alexandros Partonas on 22.04.18.
//  Copyright © 2018 Alexandros Partonas. All rights reserved.
//

import Foundation

struct User: Decodable {
    let login: String
    let id: Int
    let avatarUrl: URL
    let url: URL
    let reposUrl: URL
    let type: UserType
    
    private enum CodingKeys: String, CodingKey {
        case login
        case id
        case avatarUrl = "avatar_url"
        case url
        case reposUrl = "repos_url"
        case type
    }
}

enum UserType: String, Decodable {
    case user = "User"
    case organization = "Organization"
}
