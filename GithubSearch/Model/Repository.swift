//
//  Repository.swift
//  GithubSearch
//
//  Created by Alexandros Partonas on 22.04.18.
//  Copyright © 2018 Alexandros Partonas. All rights reserved.
//

import Foundation

struct Repository: Decodable {
    let id: Int
    let name: String
    let owner: User
    let description: String?
    let subscribers_url: URL
    let forks: Int
}
