//
//  EmptyStateView.swift
//  GithubSearch
//
//  Created by Alexandros Partonas on 22.04.18.
//  Copyright © 2018 Alexandros Partonas. All rights reserved.
//

import UIKit
import Cartography

class NoContentStateView: UIView {
    
    var info: String = "" {
        didSet {
            messageLabel.text = info
        }
    }
    
    var isLoading: Bool = false {
        didSet {
            messageLabel.isHidden = isLoading
            guard isLoading else {
                activityIndicator.stopAnimating(); return
            }
            activityIndicator.startAnimating()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(messageLabel)
        addSubview(activityIndicator)
        constrainSubviews()
    }
    
    // MARK: - Autolayout
    private func constrainSubviews() {
        constrain(messageLabel, self) {
            messageLabel, superview in
            
            messageLabel.centerX == superview.centerX
            messageLabel.centerY == superview.centerY
            messageLabel.width == superview.width - CGFloat(32)
        }
        
        constrain(activityIndicator, self) {
            activityIndicator, superview in
            
            activityIndicator.centerX == superview.centerX
            activityIndicator.centerY == superview.centerY
        }
    }
    
    // MARK: - Subview
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.medium)
        return label
    }()
    
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator.hidesWhenStopped = true
        return indicator
    }()
    
    // MARK: - Not implemented
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
