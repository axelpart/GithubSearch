//
//  RepoDetailsTableViewCell.swift
//  GithubSearch
//
//  Created by Alexandros Partonas on 23.04.18.
//  Copyright © 2018 Alexandros Partonas. All rights reserved.
//

import Foundation
import Cartography
import Kingfisher

class RepoDetailsTableViewCell: UITableViewCell {
    
    var viewModel: RepoDetailsCellViewModel? {
        didSet {
            guard let viewModel = viewModel else { return }
            userImageView.kf.setImage(with: viewModel.userImageUrl, options: [.transition(.fade(0.2))])
            userLoginLabel.text = viewModel.loginName
        }
    }
    
    // MARK: - View lifecycle
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(userImageView)
        contentView.addSubview(userLoginLabel)
        
        constrainSubviews()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        userImageView.kf.cancelDownloadTask()
    }
    
    // MARK: - Autolayout
    private func constrainSubviews() {
        constrain(userImageView, userLoginLabel, contentView) {
            userImageView, userLoginLabel, superview in
            
            userImageView.left == superview.left + CGFloat(16)
            userImageView.centerY == superview.centerY
            userImageView.width == CGFloat(50)
            userImageView.height == CGFloat(50)
            
            userLoginLabel.centerY == userImageView.centerY
            userLoginLabel.left == userImageView.right + CGFloat(16)
            userLoginLabel.right == superview.right - CGFloat(16)
        }
    }
    
    // MARK: - Subviews
    private lazy var userImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    private lazy var userLoginLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        return label
    }()
    
    // MARK: Not implemented
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
