//
//  RepoDetailsViewController.swift
//  GithubSearch
//
//  Created by Alexandros Partonas on 23.04.18.
//  Copyright © 2018 Alexandros Partonas. All rights reserved.
//

import UIKit

class RepoDetailsTableViewController: UITableViewController {
    
    fileprivate let viewModel: RepoDetailsViewModel
    fileprivate let cellIdentifier = "RepoDetailsTableViewCell"
 
    init(viewModel: RepoDetailsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = viewModel.pageTitle
        setupTableview()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        noContentStateView.isLoading = true
        viewModel.getSubscribers { [weak self] subscribers, error in
            self?.noContentStateView.isLoading = false
            self?.updateView(with: subscribers, error: error)
        }
    }
    
    // MARk: - Update view
    private func updateView(with subscribers: [User]?, error: Error?) {
        guard error == nil else {
            showBasicAlert(withTitle: "Something went wrong", andBody: error!.localizedDescription); return
        }
        
        guard let subscribers = subscribers, subscribers.count != 0 else {
            noContentStateView.info = "No subscribers found for this repository"
            return
        }
        tableView.reloadData()
    }
    
    // MARK: - Setup helper
    private func setupTableview() {
        tableView.tableFooterView = UIView()
        tableView.register(RepoDetailsTableViewCell.self, forCellReuseIdentifier: cellIdentifier)
    }
    
    // MARK: - Subviews
    private lazy var noContentStateView: NoContentStateView = {
        let stateView = NoContentStateView(frame: view.frame)
        stateView.isLoading = true
        return stateView
    }()
    
    // MARK: - Not implemented
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension RepoDetailsTableViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView = viewModel.numberOfItems > 0 ? nil : noContentStateView
        return viewModel.numberOfItems
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell: RepoDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? RepoDetailsTableViewCell else {
            return UITableViewCell() }
        cell.viewModel = viewModel.createDetailsCellViewModel(at: indexPath.row)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.sectionTitle
    }
}
