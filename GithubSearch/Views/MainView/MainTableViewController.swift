//
//  MainViewController.swift
//  GithubSearch
//
//  Created by Alexandros Partonas on 22.04.18.
//  Copyright © 2018 Alexandros Partonas. All rights reserved.
//

import UIKit

class MainTableViewController: UITableViewController {
    
    fileprivate let viewModel = MainTableViewModel(mainDataProtocol: MainDataManager())
    fileprivate let cellIdentifier = "MainTableViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableview()
        setupNavigationTitle()
    }
    
    // MARK: - Setup helper
    private func setupTableview() {
        tableView.estimatedRowHeight = 120
        tableView.tableFooterView = UIView()
        tableView.register(MainTableViewCell.self, forCellReuseIdentifier: cellIdentifier)
    }
    
    private func setupNavigationTitle() {
        navigationItem.title = "Github search"
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = false
        } else {
            navigationItem.titleView = searchController.searchBar
            searchController.hidesNavigationBarDuringPresentation = false
        }
    }
    
    // MARK: - Subviews
    fileprivate lazy var searchController: UISearchController = {
        let vc = UISearchController(searchResultsController: nil)
        vc.searchBar.delegate = self
        vc.obscuresBackgroundDuringPresentation = false
        vc.definesPresentationContext = true
        vc.dimsBackgroundDuringPresentation = false
        return vc
    }()
    
    private lazy var noContentStateView: NoContentStateView = {
        let sateView = NoContentStateView(frame: view.frame)
        sateView.info = "Please use search bar to find repository you might like!"
        return sateView
    }()
}

extension MainTableViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView = viewModel.numberOfItems > 0 ? nil : noContentStateView
        return viewModel.numberOfItems
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell: MainTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? MainTableViewCell else {
            return UITableViewCell() }
        cell.viewModel = viewModel.createCellViewModel(at: indexPath.row)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let repoDetailsViewModel = viewModel.createRepoDetailsViewModel(for: indexPath.row)
        let repoDetailsVC = RepoDetailsTableViewController(viewModel: repoDetailsViewModel)
        searchController.isActive = false
        navigationController?.pushViewController(repoDetailsVC, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

// MARK: - UISearchBar delegate
extension MainTableViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        viewModel.shouldCleanData = true
        tableView.reloadData()
        noContentStateView.isLoading = true
        
        guard let text = searchBar.text, !text.isEmptyOrOnlyWhitespaces else { return }
        viewModel.searchGithubForRepositories(with: text) { [weak self] repositories, error in
            self?.searchController.isActive = false
            self?.noContentStateView.isLoading = false
            self?.updateView(repositories: repositories, error: error)
        }
    }
    
    private func updateView(repositories: [Repository]?, error: Error?) {
        guard error == nil else {
            showBasicAlert(withTitle: "Something went wrong", andBody: error!.localizedDescription); return
        }
        
        guard let repos = repositories, repos.count != 0 else {
            noContentStateView.info = "Sorry, we couldn't find any matching result. Please try again."
            return
        }
        tableView.reloadData()
    }
}
