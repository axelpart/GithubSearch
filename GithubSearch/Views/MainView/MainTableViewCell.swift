//
//  MainTableViewCell.swift
//  GithubSearch
//
//  Created by Alexandros Partonas on 22.04.18.
//  Copyright © 2018 Alexandros Partonas. All rights reserved.
//

import UIKit
import Cartography
import Kingfisher

class MainTableViewCell: UITableViewCell {
    
    var viewModel: MainTableViewCellViewModel? {
        didSet {
            guard let viewModel = viewModel else { return }
            avatarImageView.kf.setImage(with: viewModel.userAvatarUrl, options: [.transition(.fade(0.2))])
            repositoryNameLabel.text = viewModel.repoName
            repositoryDescriptionLabel.text = viewModel.repoDescription
            repositoryForksLabel.text = viewModel.forksCountString
        }
    }
    
    // MARK: - View lifecycle
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(avatarImageView)
        contentView.addSubview(repositoryNameLabel)
        contentView.addSubview(repositoryDescriptionLabel)
        contentView.addSubview(repositoryForksLabel)
        
        constrainSubviews()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        avatarImageView.kf.cancelDownloadTask()
    }
    
    // MARK: - Autolayout
    private func constrainSubviews() {
        constrain(avatarImageView, repositoryNameLabel, contentView) {
            avatarImageView, repositoryNameLabel, superview in
            
            avatarImageView.left == superview.left + CGFloat(16)
            avatarImageView.centerY == superview.centerY
            avatarImageView.width == CGFloat(50)
            avatarImageView.height == CGFloat(50)
            
            repositoryNameLabel.top == superview.top + CGFloat(16)
            repositoryNameLabel.left == avatarImageView.right + CGFloat(16)
            repositoryNameLabel.right == superview.right - CGFloat(16)
        }
        
        constrain(repositoryNameLabel, repositoryDescriptionLabel, repositoryForksLabel, contentView) {
            repositoryNameLabel, repositoryDescriptionLabel, repositoryForksLabel, superview in
            
            repositoryDescriptionLabel.top == repositoryNameLabel.bottom + CGFloat(10)
            repositoryDescriptionLabel.left == repositoryNameLabel.left
            repositoryDescriptionLabel.right == repositoryNameLabel.right
            
            repositoryForksLabel.top == repositoryDescriptionLabel.bottom + CGFloat(10)
            repositoryForksLabel.left == repositoryDescriptionLabel.left
            repositoryForksLabel.right == repositoryDescriptionLabel.right
            repositoryForksLabel.bottom == superview.bottom - CGFloat(16)
        }
    }
    
    // MARK: - Subviews
    private lazy var avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    private lazy var repositoryNameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        return label
    }()
    
    private lazy var repositoryDescriptionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var repositoryForksLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    // MARK: Not implemented
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
