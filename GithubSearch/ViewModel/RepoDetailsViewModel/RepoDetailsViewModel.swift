//
//  RepoDetailsViewModel.swift
//  GithubSearch
//
//  Created by Alexandros Partonas on 23.04.18.
//  Copyright © 2018 Alexandros Partonas. All rights reserved.
//

import Foundation

class RepoDetailsViewModel {
    
    private let detailsDataProtocol: DetailsDataProtocol
    private let repository: Repository
    private var subscribers: [User] = []
    
    var pageTitle: String {
        return repository.name
    }
    
    // MARK: - Tableview dataSource
    var numberOfItems: Int {
        return subscribers.count
    }
    
    var sectionTitle: String {
        return subscribers.count > 0 ? "\(subscribers.count) subscribers in total" : ""
    }
    
    init(repository: Repository, detailsDataProtocol: DetailsDataProtocol) {
        self.detailsDataProtocol = detailsDataProtocol
        self.repository = repository
    }
    
    // MARK: - Get subscribers
    func getSubscribers(_ completion: @escaping (_ subscribers: [User]?, _ error: Error?) -> ()) {
        detailsDataProtocol.getSubscribers(forOwner: repository.owner.login, andRepoName: repository.name) { [weak self] subscribers, error in
            if let subscribers = subscribers {
                self?.subscribers = subscribers
                completion(subscribers, nil)
            } else if let error = error {
                completion(nil, error)
            }
        }
    }
    
    // MARK: - Cell viewModels
    func createDetailsCellViewModel(at index: Int) -> RepoDetailsCellViewModel {
        let subscriber = subscribers[index]
        return RepoDetailsCellViewModel(subscriber: subscriber)
    }
}
