//
//  RepoDetailsTableViewCellViewModel.swift
//  GithubSearch
//
//  Created by Alexandros Partonas on 23.04.18.
//  Copyright © 2018 Alexandros Partonas. All rights reserved.
//

import Foundation

class RepoDetailsCellViewModel {
    
    private let subscriber: User
    
    var loginName: String {
        return subscriber.login
    }
    
    var userImageUrl: URL {
        return subscriber.avatarUrl
    }
    
    init(subscriber: User) {
        self.subscriber = subscriber
    }
}
