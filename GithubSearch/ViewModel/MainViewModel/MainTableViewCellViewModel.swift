//
//  MainTableViewCellViewModel.swift
//  GithubSearch
//
//  Created by Alexandros Partonas on 22.04.18.
//  Copyright © 2018 Alexandros Partonas. All rights reserved.
//

import Foundation

class MainTableViewCellViewModel {
    
    private let repository: Repository
    
    var repoName: String {
        return "Repo name: \(repository.name)"
    }
    
    var repoDescription: String {
        return "Repo description:\n\(repository.description ?? "No description provided")"
    }
    
    var userAvatarUrl: URL {
        return repository.owner.avatarUrl
    }
    
    var forksCountString: String {
        return "Forks: \(String(repository.forks))"
    }
    
    init(repository: Repository) {
        self.repository = repository
    }
}
