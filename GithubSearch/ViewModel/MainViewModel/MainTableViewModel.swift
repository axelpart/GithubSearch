//
//  MainTableViewModel.swift
//  GithubSearch
//
//  Created by Alexandros Partonas on 22.04.18.
//  Copyright © 2018 Alexandros Partonas. All rights reserved.
//

import Foundation

class MainTableViewModel {
    
    private let mainDataProtocol: MainDataProtocol
    private var repositories: [Repository] = []
    
    // MARK: - Tableview dataSource
    var numberOfItems: Int {
        return repositories.count
    }
    
    var shouldCleanData: Bool = false {
        didSet {
            repositories = []
        }
    }
    
    init(mainDataProtocol: MainDataProtocol) {
        self.mainDataProtocol = mainDataProtocol
    }
    
    // MARK: - Search Github Api
    func searchGithubForRepositories(with text: String, completion: @escaping (_ repositories: [Repository]?, _ error: Error?) -> ()) {
        mainDataProtocol.searchGithub(with: text) { [weak self] repositories, error in
            if let repositories = repositories {
                self?.repositories = repositories
                completion(repositories, nil)
            } else if let error = error {
                completion(nil, error)
            }
        }
    }
    
    // MARK: - Cell viewModels
    func createCellViewModel(at index: Int) -> MainTableViewCellViewModel {
        let repository = repositories[index]
        return MainTableViewCellViewModel(repository: repository)
    }
    
    func createRepoDetailsViewModel(for index: Int) -> RepoDetailsViewModel {
        let repository = repositories[index]
        return RepoDetailsViewModel(repository: repository, detailsDataProtocol: DetailsDataManager())
    }
}
