//
//  Decodable+Extensions.swift
//  GithubSearch
//
//  Created by Alexandros Partonas on 22.04.18.
//  Copyright © 2018 Alexandros Partonas. All rights reserved.
//

import Foundation

extension Decodable {
    init(decoding data: Data) throws {
        self = try JSONDecoder().decode(Self.self, from: data)
    }
    
    static func decoded(from data: Data) -> (Self?, Error?) {
        do {
            let decoded = try Self(decoding: data)
            return (decoded, nil)
        } catch {
            return (nil, error)
        }
    }
}
