//
//  String+Extensions.swift
//  GithubSearch
//
//  Created by Alexandros Partonas on 22.04.18.
//  Copyright © 2018 Alexandros Partonas. All rights reserved.
//

import Foundation

extension String {
    
    var isEmptyOrOnlyWhitespaces: Bool {
        guard isEmpty else {
            return trimmingCharacters(in: .whitespaces).isEmpty
        }
        return true
    }
}
