//
//  UIViewController+Extensions.swift
//  GithubSearch
//
//  Created by Alexandros Partonas on 22.04.18.
//  Copyright © 2018 Alexandros Partonas. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showBasicAlert(withTitle title: String, andBody body: String) {
        let alert = UIAlertController(title: title, message: body, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        let topVC = topViewController()
        topVC.present(alert, animated: true, completion: nil)
    }
    
    func topViewController() -> UIViewController {
        if presentedViewController == nil {
            return self
        }
        
        if let navigation = presentedViewController as? UINavigationController {
            return navigation.visibleViewController!.topViewController()
        }
        
        if let tab = presentedViewController as? UITabBarController {
            guard let selectedTab = tab.selectedViewController else {
                return tab.topViewController()
            }
            return selectedTab.topViewController()
        }
        return presentedViewController!.topViewController()
    }
}
