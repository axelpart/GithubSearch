//
//  JsonHelper.swift
//  GithubSearchTests
//
//  Created by Alexandros Partonas on 22.04.18.
//  Copyright © 2018 Alexandros Partonas. All rights reserved.
//

import Foundation

class JsonHelper {
    
    static let sharedInstance = JsonHelper()
    
    func getJsonData(fromFile fileName: String) -> Data {
        guard let jsonData = getJsonData(withName: fileName) else {
            fatalError("Unable to convert \(fileName).json to NSData")
        }
        return jsonData
    }
    
    private func getJsonData(withName name: String) -> Data? {
        guard let pathString = Bundle(for: type(of: self)).path(forResource: name, ofType: "json") else {
            fatalError("\(name).json not found")
        }
        
        guard let jsonString = try? NSString(contentsOfFile: pathString, encoding: String.Encoding.utf8.rawValue) else {
            fatalError("Unable to convert \(name).json to String")
        }
        
        guard let jsonData = jsonString.data(using: String.Encoding.utf8.rawValue) else {
            fatalError("Unable to convert \(name).json to NSData")
        }
        return jsonData
    }
}
