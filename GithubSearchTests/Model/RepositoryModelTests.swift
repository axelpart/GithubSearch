//
//  RepositoryModelTests.swift
//  GithubSearchTests
//
//  Created by Alexandros Partonas on 22.04.18.
//  Copyright © 2018 Alexandros Partonas. All rights reserved.
//

import XCTest
import Nimble
@testable import GithubSearch

class RepositoryModelTests: XCTestCase {
    
    func testRepositoryModel() {
        let jsonData = JsonHelper().getJsonData(fromFile: "MockRepositoriesResponse")
        
        do {
            let searchResponse: SearchResponse = try JSONDecoder().decode(SearchResponse.self, from: jsonData)
            expect(searchResponse.items.count).to(equal(9), description: "Repositories count is not equal 9")
            
            guard let repository = searchResponse.items.first else {
                XCTAssert(false); return
            }
            expect(repository.id).to(equal(95875527), description: "Repository id is not equal 477610")
            expect(repository.name).to(equal("ARTetris"), description: "Repository name is not ARTetris")
            expect(repository.owner.id).to(equal(10406525), description: "Repository's owner id is not equal 10406525")
            expect(repository.description).to(equal("Augmented Reality Tetris made with ARKit and SceneKit"), description: "Repository description is wrong")
            expect(repository.subscribers_url).to(equal(URL(string: "https://api.github.com/repos/exyte/ARTetris/subscribers")), description: "Repository subscribers_url is not https://api.github.com/repos/exyte/ARTetris/subscribers")
            expect(repository.forks).to(equal(135), description: "Repository forks count is not 135")
        } catch {
            print("\(error)")
            XCTAssert(false)
        }
    }
}
