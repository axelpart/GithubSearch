//
//  UserModelTests.swift
//  GithubSearchTests
//
//  Created by Alexandros Partonas on 22.04.18.
//  Copyright © 2018 Alexandros Partonas. All rights reserved.
//

import XCTest
import Nimble
@testable import GithubSearch

class UserModelTests: XCTestCase {
    
    func testUserModel() {
        let jsonData = JsonHelper().getJsonData(fromFile: "MockUsersData")
        
        do {
            let users: [User] = try JSONDecoder().decode([User].self, from: jsonData)
            guard let user = users.first else { XCTAssert(false); return }
            expect(user.login).to(equal("ystrot"), description: "User login is not ystrot")
            expect(user.id).to(equal(477610), description: "User id is not equal 477610")
            expect(user.avatarUrl).to(equal(URL(string: "https://avatars2.githubusercontent.com/u/477610?v=4")), description: "User avatarUrl is not https://avatars2.githubusercontent.com/u/477610?v=4")
            expect(user.url).to(equal(URL(string: "https://api.github.com/users/ystrot")), description: "User url is not equal https://api.github.com/users/ystrot")
            expect(user.reposUrl).to(equal(URL(string: "https://api.github.com/users/ystrot/repos")), description: "User reposUrl is not https://api.github.com/users/ystrot/repos")
            expect(user.type.rawValue).to(equal("User"), description: "User type is not user")
        } catch {
            print("\(error)")
            XCTAssert(false)
        }
    }
}
