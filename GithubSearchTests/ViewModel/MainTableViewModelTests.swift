//
//  MainViewModelTests.swift
//  GithubSearchTests
//
//  Created by Alexandros Partonas on 22.04.18.
//  Copyright © 2018 Alexandros Partonas. All rights reserved.
//

import XCTest
import Nimble
@testable import GithubSearch

class MainTableViewModelTests: XCTestCase {

    private var viewModel: MainTableViewModel?
    private let dataManager = MainDataManagerMock()
    
    override func setUp() {
        super.setUp()
        viewModel = MainTableViewModel(mainDataProtocol: dataManager)
    }
    
    override func tearDown() {
        viewModel = nil
        super.tearDown()
    }
    
    // MARK: - Search Github
    private var searchGithubDidComplete = false
    func testSearchGithubSuccessful() {
        dataManager.shouldFailCallSearchGithub = false
        viewModel?.searchGithubForRepositories(with: "test", completion: { repositories, error in
            expect(repositories?.count).to(beGreaterThan(1), description: "Repositories array should contain data")
            expect(error).to(beNil(), description: "Error should be nil")
            self.searchGithubDidComplete = true
        })
        searchGithubCompleted()
    }
    
    func testSearchGithubUnsuccessful() {
        dataManager.shouldFailCallSearchGithub = true
        viewModel?.searchGithubForRepositories(with: "test", completion: { repositories, error in
            expect(error).toNot(beNil(), description: "Error should not be nil")
            expect(repositories).to(beNil(), description: "Repositories should be nil")
            self.searchGithubDidComplete = true
        })
        searchGithubCompleted()
    }
    
    private func searchGithubCompleted() {
        expect(self.dataManager.didCallSearchGithub).to(equal(true), description: "Search github function was not called")
        expect(self.searchGithubDidComplete).to(equal(true), description: "Search github function did not complete")
    }
}
