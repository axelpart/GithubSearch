//
//  RepoDetailsViewModelTests.swift
//  GithubSearchTests
//
//  Created by Alexandros Partonas on 23.04.18.
//  Copyright © 2018 Alexandros Partonas. All rights reserved.
//

import XCTest
import Nimble
@testable import GithubSearch

class RepoDetailsViewModelTests: XCTestCase {
    
    private var viewModel: RepoDetailsViewModel?
    private let dataManager = DetailsDataManagerMock()
    
    override func setUp() {
        super.setUp()
        viewModel = RepoDetailsViewModel(repository: dataManager.repository, detailsDataProtocol: dataManager)
    }
    
    override func tearDown() {
        viewModel = nil
        super.tearDown()
    }
    
    // MARK: - Search Github
    private var getSubscribersDidComplete = false
    func testGetSubscribersSuccessful() {
        dataManager.shouldFailCallGetSubscribers = false
        viewModel?.getSubscribers({ (subscribers, error) in
            expect(subscribers?.count).to(beGreaterThan(1), description: "Subscribers array should contain data")
            expect(error).to(beNil(), description: "Error should be nil")
            self.getSubscribersDidComplete = true
        })
        getSubscribersCompleted()
    }
    
    func testGetSubscribersUnsuccessful() {
        dataManager.shouldFailCallGetSubscribers = true
        viewModel?.getSubscribers({ (subscribers, error) in
            expect(error).toNot(beNil(), description: "Error should not be nil")
            expect(subscribers).to(beNil(), description: "Subscribers array should be nil")
            self.getSubscribersDidComplete = true
        })
        getSubscribersCompleted()
    }
    
    private func getSubscribersCompleted() {
        expect(self.dataManager.didCallGetSubscribers).to(equal(true), description: "Get repository subscribers function was not called")
        expect(self.getSubscribersDidComplete).to(equal(true), description: "Get subscribers function did not complete")
    }
}
