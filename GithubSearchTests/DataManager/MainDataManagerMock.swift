//
//  MainDataManagerMock.swift
//  GithubSearchTests
//
//  Created by Alexandros Partonas on 22.04.18.
//  Copyright © 2018 Alexandros Partonas. All rights reserved.
//

import Foundation
@testable import GithubSearch

class MainDataManagerMock: MainDataProtocol {
    
    private var genericError = NSError(domain: "searchGithub-error", code: 50000, userInfo: nil)
    
    var repositories: [Repository] {
        let jsonData = JsonHelper().getJsonData(fromFile: "MockRepositoriesResponse")
        guard let searchResponse = try? JSONDecoder().decode(SearchResponse.self, from: jsonData) else { return [] }
        return searchResponse.items
    }
    
    var didCallSearchGithub = false
    var shouldFailCallSearchGithub = false
    func searchGithub(with text: String, completion: @escaping (_ repositories: [Repository]?, _ error: Error?) -> ()) {
        didCallSearchGithub = true
        if shouldFailCallSearchGithub {
            completion(nil, genericError)
        } else {
            completion(repositories, nil)
        }
    }
}
