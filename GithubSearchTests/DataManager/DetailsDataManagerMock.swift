//
//  DetailsDataManagerMock.swift
//  GithubSearchTests
//
//  Created by Alexandros Partonas on 23.04.18.
//  Copyright © 2018 Alexandros Partonas. All rights reserved.
//

import Foundation
@testable import GithubSearch

class DetailsDataManagerMock: DetailsDataProtocol {
    
    private var genericError = NSError(domain: "getSubscribers-error", code: 10000, userInfo: nil)
    
    var repository: Repository {
        let jsonData = JsonHelper().getJsonData(fromFile: "MockRepositoryData")
        let repository = try! JSONDecoder().decode(Repository.self, from: jsonData)
        return repository
    }
    
    var subsribers: [User] {
        let jsonData = JsonHelper().getJsonData(fromFile: "MockUsersData")
        guard let users = try? JSONDecoder().decode([User].self, from: jsonData) else { return [] }
        return users
    }
    
    var didCallGetSubscribers = false
    var shouldFailCallGetSubscribers = false
    func getSubscribers(forOwner login: String, andRepoName name: String, completion: @escaping ([User]?, Error?) -> ()) {
        didCallGetSubscribers = true
        if shouldFailCallGetSubscribers {
            completion(nil, genericError)
        } else {
            completion(subsribers, nil)
        }
    }
}
